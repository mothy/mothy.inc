<?php
$app->post("/comenzar/", function () use ($app){
    $st = $app->db->prepare('SELECT * FROM usuarios where correo = ?');
    $st->setFetchMode(PDO::FETCH_OBJ);
    $correo = $_POST['correo'];
    $st->execute(array($correo));
    $user = $st->fetch();
  if (!empty($user) && $_POST['pass'] == $user->pass) {
    $_SESSION['status'] = true;
    $_SESSION['user'] = $user->nombre;
    return $app->redirect($app->urlFor('casa'));
  } else {
    $app->flash('error','Usuario y/o contraseña incorrecta');
    $app->flashKeep();
    return $app->redirect($app->urlFor('comenzar'));
  }
  
})->name('loging');

$app->get('/home/',function () use ($app){
  $data['status'] = $_SESSION['status'];
  $data['u'] = $_SESSION['user'];
  return $app->render('comenzar.twig',$data);
})->name('casa');

$app->get("/log/", function () use($app){
    return $app->render('login.twig');
})->name('comenzar');
$app->get('/Registro/', function () use($app){
  return $app->render('registro.twig');
})->name('registro');
$app->post('/Registrado/', function () use($app){
$nombre = $_POST['nombre'];
$apellidos = $_POST['apellido'];
$date = new DateTime('now');
$fecha = $date->format('Y-m-d');
$cumple = $_POST['cumple'];
$correo = $_POST['correo'];
$pass = $_POST['clave'];
$conpass = $_POST['conclave'];
try {
  function parms($string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="'$v'";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return $string;
    }
  $arr = array($nombre,$apellidos,$fecha,$cumple,$correo,$pass);
if ($_POST['clave'] === $_POST['conclave'] && !empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['clave'])) {
  print parms("INSERT INTO usuarios(nombre,apellidos,fecha_registro,cumpleanios,correo,pass) VALUES (?,?,?,?,?,?)",$arr);
  $st = $app->db->prepare("INSERT INTO usuarios(nombre,apellidos,fecha_registro,cumpleanios,correo,pass) VALUES (?,?,?,?,?,?)");
  $st->setFetchMode(PDO::FETCH_OBJ);
  $st->execute($arr);
  $app->flash('noreg','registro exitoso');
  $app->flashKeep();
  return $app->redirect($app->urlFor('comenzar'));
} else {
  $app->flash('noreg','algo ha salido mal');
  $app->flashKeep();
  return $app->redirect($app->urlFor('registro'));
}
  } catch (Exception $e) {
      print $e->getMessage()."en la linea".$e->getLine();
  }

})->name('registrar');
 ?>
