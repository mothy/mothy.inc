<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/configuracion.php';

$has_db = function ($app) {
  return function () use ($app) {
    if(is_null($app->db)){
      return "o ño";
    } else {
      return true;
    }
  };
};

$is_logged = function ($app) {
  return function () use ($app) {
    if(isset($app->session['usuario']) and !is_null($app->session['usuario'])){
      return true;
    } else {
      return $app->redirect($app->urlFor('home'));
    }
  };
};

$app->get('/', $has_db($app), function() use($app) {
    return $app->render('casa.twig');
})->name('home');

$app->get('/logout/', function() use($app){
  if(isset($app->session['all_data'])){
    session_unset();
    session_destroy();
    unset($_SESSION['status']);
  }
  $app->view()->setData('user', null);
  $_SESSION['status'] = false;
  $app->redirect($app->urlFor('home'));
})->name('logout');

include_once CONTROLLERS_DIR.'contenido.php';

$app->run();
 ?>
