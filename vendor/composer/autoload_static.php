<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc3c406b08def2a19b4fc97f5d4efca4a
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Slim\\Views\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Slim\\Views\\' => 
        array (
            0 => __DIR__ . '/..' . '/slim/views',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
        'S' => 
        array (
            'Slim' => 
            array (
                0 => __DIR__ . '/..' . '/slim/slim',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc3c406b08def2a19b4fc97f5d4efca4a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc3c406b08def2a19b4fc97f5d4efca4a::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitc3c406b08def2a19b4fc97f5d4efca4a::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
