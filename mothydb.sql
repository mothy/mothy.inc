CREATE TABLE usuarios(
  id SERIAL,
  nombre text,
  apellidos text,
  fecha_registro timestamp,
  cumpleanios date,
  correo text,
  pass text
);
ALTER TABLE usuarios ALTER column fecha_registro type datetime;
